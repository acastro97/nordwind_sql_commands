
# Nordwind DB SQL commands

##### These are my queries used to explore the Nordwind database:

### Query #1

![Command_1](./Screenshots/1.png "Command_1")

This query is to get the Company Name from the Suppliers table, the ProductName, QuantityPerUnit, UnitsInStock from the Products table and the UnitPrice,Quantity and Discount from the OrderDetails table.

### Output:

![Command_1_Output](./Screenshots/1.1.png "Command_1_Output")


### Query #2

![Command_2](./Screenshots/2.png "Command_2")

The main goal of this query is to get the top 10 of countries with the highest freight.

### Output:

![Command_2_Output](./Screenshots/2.2.png "Command_2_Output")


### Other commands used on the queries: 

- **AS:** This command will allow us to set an alias to the tables, this with the main goal to have a better organization in case
	of exist variables with the same name on different tables
- **JOIN:** With this command we going obtain the values from different tables using the IDs as a reference index
- **ORDER BY:**  This command is used to organize the the query output acording to the conditions defined in the query 
- **DESC:** This command will allow us organize the output from the highest variable value to the lowest
- **TOP:** This command is used to define the quantity of values that you want in your output
- **GROUP BY:** This command is used to group the values according an specific table value. This value can only be used once exist a 	calculate value in your query
- **SUM:** This command is used to do summatories of numerical values